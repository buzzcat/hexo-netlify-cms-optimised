# Hexo + Netlify CMS Optimised Starter

[![Netlify Status](https://api.netlify.com/api/v1/badges/69d7432c-312e-4e7b-84ff-86f08c21780a/deploy-status)](https://app.netlify.com/sites/hnco/deploys)

An **[optimised example (click me to see demo)](https://hnco.buzzcat.com/)** that is built with [Hexo](https://hexo.io/) on top of [Netlify](https://netlify.com) + [Netlify CMS](https://www.netlifycms.org/). 

## What do you get

Out of the box you get a hexo install with the default theme 'landscape' (easy to add another theme).

You get Netlify CMS for easily writing pages and posts straight from your browser.

You get optimisations that will give you a 98-100 lighthouse performance score. 

You get a basic setup for a progressive web app.

You get some tweaks for SEO. 

You also get lazy loaded images delivered at the correct sizes.

## Getting Started

Use the deploy button below to boot up a site straight away on Netlify.

<a href="https://app.netlify.com/start/deploy?repository=https://gitlab.com/buzzcat/hexo-netlify-cms-optimised&amp;stack=cms"><img src="https://www.netlify.com/img/deploy/button.svg" alt="Deploy to Netlify"></a>

You’ll be asked to connect to GitLab (so get a free GitLab account).

Netlify will then automatically create a repository in your GitLab account with a copy of the files from here.

Give it a minute and the new site will be ready on Netlify.

Enable netlify git-gateway  
![](imgs/git-gateway.png)  

Add netlify-identity-widget.js   
code is `<script src="https://identity.netlify.com/v1/netlify-identity-widget.js"></script>`  
![](imgs/snippet.png)

**Note: set registration preferences to invite only if you want to keep the riff-raff out**

Now, Netlify CMS is available at `your-site/admin`

### Play around locally

```
$ git clone https://gitlab.com/buzzcat/hexo-netlify-cms-optimised
$ cd hexo-netlify-cms-optimised
$ npm i
$ hexo generate
$ hexo s
```

*Thrown together by [Scott Adams](https://buzzcat.com/). All credit goes to the maintainers and contributors of the following:*

[hexo](https://github.com/hexojs/hexo) [![Star on GitHub](https://img.shields.io/github/stars/hexojs/hexo.svg?style=social)](https://github.com/hexojs/hexo/stargazers)

[hexo-filter-optimize](https://github.com/theme-next/hexo-filter-optimize) [![Star on GitHub](https://img.shields.io/github/stars/theme-next/hexo-filter-optimize.svg?style=social)](https://github.com/theme-next/hexo-filter-optimize/stargazers)

[hexo-nofollow](https://github.com/weyusi/hexo-nofollow) [![Star on GitHub](https://img.shields.io/github/stars/weyusi/hexo-nofollow.svg?style=social)](https://github.com/weyusi/hexo-nofollow/stargazers)

[hexo-autoprefixer](https://github.com/hexojs/hexo-autoprefixer) [![Star on GitHub](https://img.shields.io/github/stars/hexojs/hexo-autoprefixer.svg?style=social)](https://github.com/hexojs/hexo-autoprefixer/stargazers)

[hexo-generator-seo-friendly-sitemap](https://github.com/ludoviclefevre/hexo-generator-seo-friendly-sitemap) [![Star on GitHub](https://img.shields.io/github/stars/ludoviclefevre/hexo-generator-seo-friendly-sitemap.svg?style=social)](https://github.com/ludoviclefevre/hexo-generator-seo-friendly-sitemap/stargazers)

[hexo-pwa](https://github.com/lavas-project/hexo-pwa) [![Star on GitHub](https://img.shields.io/github/stars/lavas-project/hexo-pwa.svg?style=social)](https://github.com/lavas-project/hexo-pwa/stargazers)

[hexo-lazysizes](https://gitlab.com/buzzcat/hexo-lazysizes) [![Star on GitHub](https://img.shields.io/github/stars/buzzcat-com/hexo-netlify-cms-optimised.svg?style=social)](https://github.com/buzzcat-com/hexo-netlify-cms-optimised/stargazers)

[hexo-all-minifier](https://github.com/chenzhutian/hexo-all-minifier) [![Star on GitHub](https://img.shields.io/github/stars/chenzhutian/hexo-all-minifier.svg?style=social)](https://github.com/chenzhutian/hexo-all-minifier/stargazers)

[netlify-cms](https://github.com/netlify/netlify-cms) [![Star on GitHub](https://img.shields.io/github/stars/netlify/netlify-cms.svg?style=social)](https://github.com/netlify/netlify-cms/stargazers)

[hexo-netlify-cms](https://github.com/JiangTJ/hexo-netlify-cms) [![Star on GitHub](https://img.shields.io/github/stars/JiangTJ/hexo-netlify-cms.svg?style=social)](https://github.com/JiangTJ/hexo-netlify-cms/stargazers)


Make sure to go to each of the repos above to check out the available options and give them a star!